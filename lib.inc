section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60,
    syscall  

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
	.cycle:
	    cmp byte[rdi+rax], 0x0
		je .end
		inc rax
		jne .cycle
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    call string_length
	pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
	jns .uint
	push rdi
	mov rdi, "-"
	call print_char
	pop rdi
	neg rdi
.uint:


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, rsp 
	mov rax, rdi 
	mov rcx, 10 
	dec rsp
	mov byte[rsp], 0
.loop:
    mov rdx, 0
    div rcx 
    add rdx, 0x30 
    dec rsp
    mov byte[rsp], dl 
    test rax, rax 
    jnz .loop

    mov rdi, rsp
    push r9
    call print_string
    pop r9
    mov rsp, r9
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r10, r10
    .loop:
	mov r8b, [rdi+r10]
	mov r9b, [rsi+r10]
	cmp r8b, r9b
	jne .end
	cmp r8b, 0
	je .success
	inc r10
	jmp .loop
    .success:
	mov rax, 1
    .end:
	ret




; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
        mov r8, rdx
    dec rsp
        mov byte[rsp], 0x0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
        mov al, byte[rsp]
        inc rsp
        mov rdx, r8
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi        
    push r12
    mov r12, rdi    
    push r13
    mov r13, rsi
.loop_space:
    call read_char
    cmp rax, ' '
    je .loop_space
    cmp rax, 0x09
    je .loop_space
    cmp rax, 0xA
    je .loop_space
.loop:
    cmp rax, 0x0
    je .finally
    cmp rax, 0x20
    je .finally
    cmp rax, 0x9
    je .finally
    cmp rax, 0xA
    je .finally
    dec r13        
    cmp r13, 0
    jbe .overflow
    mov byte [r12], al
    inc r12
    call read_char
    jmp .loop
    
.finally:
    mov byte [r12], 0  
    pop r13             
    pop r12             
    mov rdi, [rsp]      
    call string_length  
    mov rdx, rax        
    pop rax             
    ret
.overflow:
    pop r13 
    pop r12
    pop rdi
    mov rax, 0
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r10, r10			
	mov rcx, 10	
	xor rax, rax
	.loop:
		xor r11, r11
		mov r11b, byte[rdi + r10]
		cmp r11b, '9'	
		ja .end			
		cmp r11b, '0'			
		jb .end					
		inc r10			
		sub r11b, '0'		
		mul rcx			
		add rax, r11		
		jmp .loop
	.end:
		mov rdx, r10		
		ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'		
	jne .uint			
	inc rdi				
	call parse_uint			
	test rdx, rdx			
	jz .not_digit			
	neg rax				
	inc rdx				
	ret
	.uint:
		jmp parse_uint		
		ret
	.not_digit:
		xor rax, rax		
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
	push rsi
	push rdx
	call string_length
	inc rax
	pop rdx
	pop rsi
	pop rdi
	cmp rax , rdx
	jg .overflow
.loop:
	mov r11, 0
	mov r11b, byte[rdi]
	mov byte[rsi], r11b
	cmp r11b, 0
	je .end
	inc rdi
	inc rsi
	jmp .loop
	
.overflow:
	xor rax, rax
	
.end:
	ret

